var singleState = {
    create: function () {
        ///about game basic
        this.EnemyLevel = 0;
        this.EnemyAppearNUM = 0;
        this.MaxEnemyPerLevel = [30, 50, 50, 1];
        this.MaxBulletNum = this.MaxEnemyPerLevel[this.MaxEnemyPerLevel.length-2]*5;
        ///about enemy
        this.EnemyGenerateTime = 0;
        this.EnemyFireTime = new Array(this.MaxEnemyPerLevel[this.MaxEnemyPerLevel.length-2]).fill(2000);
        this.BossFireTime = 0;
        ///about player
        this.PlayerFireTimer = 2000;
        this.weaponON = [false,false,false];
        this.weaponTimer = [0, 0, 0];
        this.GunFireTimer = 0;
        this.LaserFireTimer = 0;
        this.RocketFireTimer = 0;
        ///about buff
        this.MaxLevel = 10;
        this.EXPperLevel = [10,15,20,25,30,35,40,45,50,100];
        this.BuffGenerateTime = 6000;
        this.MaxNumPerBuff = [30,30,30];
        this.ShieldTime = 0;
        ///about audio
        this.BackgroundAudio = game.add.audio('BackgroundMusic', backgroundVolumn, true);
        this.FireAudio = game.add.audio('Fire', effectVolumn, false);
        this.FinalAttackAudio = game.add.audio('FinalAttack', effectVolumn, false);
        this.BoomAudio = game.add.audio('Boom', effectVolumn, false);
        this.DeadAudio = game.add.audio('Dead', effectVolumn, false);
        this.RecoverAudio = game.add.audio('Recover', effectVolumn, false);
        this.ShieldAudio = game.add.audio('Shield', effectVolumn, false);
        this.GetCoinAudio = game.add.audio('GetCoin', effectVolumn, false);
        this.BeenHitAudio = game.add.audio('BeenHit', effectVolumn, false)
        
        ///Play background music
        this.BackgroundAudio.play();
        ///Setup background
        this.background = game.add.tileSprite(0, 0, 800, 600, 'background');

        ///Setup buff
        this.recoveryBuff = game.add.group();
        this.createBuff(this.recoveryBuff, 'recovery', 0);

        this.shieldBuff = game.add.group();
        this.createBuff(this.shieldBuff, 'shield', 1);

        this.coinBuff = game.add.group();
        this.createBuff(this.coinBuff, 'coin', 2);
        this.coinBuff.forEach(coin => {coin.animations.add('coin');}, this);

        ///Setup bullets from friends
        this.bulletsFriend = game.add.group();
        this.createBullet(this.bulletsFriend, 'friendbullet', this.MaxBulletNum);

        ///Setup bullets from enemies
        this.bulletsEnemy = game.add.group();
        this.createBullet(this.bulletsEnemy, 'enemybullet', this.MaxBulletNum);

        ///Setup bullets from boss
        this.bulletsBoss = game.add.group();
        this.createBullet(this.bulletsBoss, 'bossbullet', 50);
        this.bulletsBoss.setAll('scale.x', 2);
        this.bulletsBoss.setAll('scale.y', 1);
        this.bulletsBoss.forEach(bullet => {bullet.animations.add('bossbullet');}, this);

        ///Setup bullet from weapons
        this.GunBullet = game.add.group();
        this.createBullet(this.GunBullet, 'GunBullet', this.MaxBulletNum*2);

        this.LaserBullet1 = game.add.sprite(-200, 0, 'LaserBullet');
        this.LaserBullet1.scale.setTo(1, 5);
        this.LaserBullet1.anchor.setTo(0.5, 1);
        this.LaserBullet1.angle = 90;
        this.LaserBullet1.checkWorldBounds = true;
        this.LaserBullet1.outOfBoundsKill = true;
        this.LaserBullet1.animations.add('LaserBullet');
        game.physics.enable(this.LaserBullet1, Phaser.Physics.ARCADE);

        this.LaserBullet2 = game.add.sprite(-200, 0, 'LaserBullet');
        this.LaserBullet2.scale.setTo(1, 5);
        this.LaserBullet2.anchor.setTo(0.5, 1);
        this.LaserBullet2.angle = 90;
        this.LaserBullet2.checkWorldBounds = true;
        this.LaserBullet2.outOfBoundsKill = true;
        this.LaserBullet2.animations.add('LaserBullet');
        game.physics.enable(this.LaserBullet2, Phaser.Physics.ARCADE);

        this.Rocket = game.add.group();
        this.createBullet(this.Rocket, 'Rocket', 10);

        ///Setup player
        this.player = game.add.sprite(400, 500);
        this.createPlayer(this.player);
        this.playerReady = true;
        this.playerReadyTween = game.add.tween(this.player).to({alpha:0}, 200, Phaser.Easing.Sinein, false, 0, 5, true);
        this.playerReadyTween.onComplete.add(()=>{this.playerReady = true},this);        

        ///Setup enemies
        this.enemies = game.add.group();
        this.enemies.enableBody = true;
        this.enemies.physicsBodyType = Phaser.Physics.ARCADE;
        this.enemies.createMultiple(this.MaxEnemyPerLevel[this.MaxEnemyPerLevel.length-2], 'enemy');
        this.enemies.forEach(enemy => {this.createEnemy(enemy);}, this);

        ///Setup Boss
        this.Deadboss = game.add.sprite(0, 0, 'boss').kill();
        this.Deadboss.scale.setTo(2,2);
        this.Deadboss.anchor.setTo(0.5, 1);
        this.boss = game.add.sprite().kill();
        this.createBoss(this.boss);
        this.bossRL = false;
        this.bossUD = false;
        this.bossReady = false;
        
        ///Setup weapon
        this.Gun1 = game.add.sprite(-50, 0, 'Gun');
        this.Gun1.anchor.setTo(0.5, 0.5);
        this.Gun1.angle = -90;
        this.Gun1.visible = false;
        this.Gun2= game.add.sprite(50, 0, 'Gun');
        this.Gun2.anchor.setTo(0.5, 0.5);
        this.Gun2.angle = -90;
        this.Gun2.visible = false;
        this.player.addChild(this.Gun1, 'Gun');
        this.player.addChild(this.Gun2, 'Gun');        

        this.Laser1 = game.add.sprite(-30, 0, 'Laser');
        this.Laser1.anchor.setTo(0.5, 0.5);
        this.Laser1.angle = -90;
        this.Laser1.visible = false;
        this.Laser1.addChild(this.LaserBullet1, 'LaserBullet1');
        this.player.addChild(this.Laser1, 'Laser');

        this.Laser2 = game.add.sprite(30, 0, 'Laser');
        this.Laser2.anchor.setTo(0.5, 0.5);
        this.Laser2.angle = -90;
        this.Laser2.visible = false;
        this.Laser2.addChild(this.LaserBullet2, 'LaserBullet1');
        this.player.addChild(this.Laser2, 'Laser');

        ///Setup shieldArea
        this.shieldArea = game.add.sprite(0, 0, 'shieldArea');
        game.physics.enable(this.shieldArea, Phaser.Physics.ARCADE);
        this.shieldArea.anchor.setTo(0.5, 0.5);
        this.shieldArea.alpha = 0.5;
        this.shieldArea.body.moves = false;
        this.shieldArea.visible = false;
        this.player.addChild(this.shieldArea, 'shieldArea');        

        ///Setup explosion
        this.explosions = game.add.group();
        this.explosions.createMultiple(this.MaxEnemyPerLevel[this.MaxEnemyPerLevel.length-2]+3, 'kaboom');
        this.explosions.setAll('anchor.x', 0.5);
        this.explosions.setAll('anchor.y', 0.5);
        this.explosions.forEach(boom => {boom.animations.add('kaboom');}, this);

        ///Setup emitter
        this.emitter = game.add.emitter(400, 300, 30);
        this.emitter.makeParticles('pixel');
        this.emitter.setXSpeed(-100, 100);
        this.emitter.setYSpeed(-100, 100);
        this.emitter.setScale(2, 0, 2, 0, 500);
        this.emitter.bringToTop = true;

        ///Setup start text
        this.AlreadyStart = false;
        this.start = game.add.text(400, 300, 'Level ' + (this.EnemyLevel+1), { font: '70px Arial', fill: '#fff'});
        this.start.anchor.setTo(0.5, 0.5);
        this.start.alpha = 0;
        this.startTween = game.add.tween(this.start).to({alpha:1}, 1000).to({alpha:0}, 500);

        ///Setup endding text
        this.END = false;
        this.endText1 = game.add.text(400, 300, '', { font: '70px Arial', fill: '#fff'});
        this.endText1.anchor.setTo(0.5, 0.5);
        this.endText1.visible = false;
        this.endText2 = game.add.text(400, 600, '點擊滑鼠左鍵回到目錄', { font: '25px Arial', fill: '#fff'});
        this.endText2.anchor.setTo(0.5, 1.2);
        this.endText2.alpha = 0.5;
        this.endText2.visible = false;

        ///Setup score text 
        this.score = 0;
        this.scoreString = 'Score: ';
        this.scoreText = game.add.text(10, 10, this.scoreString + this.score, { font: '25px Arial', fill: '#fff'});
        this.scoreText.alpha = 0.2;

        ///Setup coin text 
        this.coin = 1000;
        this.coinString = 'Coin: ';
        this.coinText = game.add.text(10, 40, this.coinString + this.coin, { font: '25px Arial', fill: '#fff'});
        this.coinText.alpha = 0.2;

        ///Setup level text
        this.level = 0;
        this.levelString = 'Level: ';
        this.levelText = game.add.text(10, 70, this.levelString + this.level, { font: '25px Arial', fill: '#fff'});
        this.levelText.alpha = 0.2;

        ///Setup EXP text
        this.exp = 0;
        this.expString = 'EXP: ';
        this.expText = game.add.text(10, 100, this.expString + this.exp + '%', { font: '25px Arial', fill: '#fff'});
        this.expText.alpha = 0.2;

        ///Setup lives
        this.lives = game.add.group();
        for (var i = 0 ; i < 3 ; i++) {
            var ship = this.lives.create(game.world.width - 80 + (30 * i), 25, 'heart');
            ship.anchor.setTo(0.5, 0.5);
            ship.alpha = 0.7;
        }

        ///Setup skill icon
        this.weapon1 = game.add.sprite(645, 545, 'weapon1');
        this.weapon1.alpha = 0.5;
        this.weapon2 = game.add.sprite(695, 545, 'weapon2');
        this.weapon2.alpha = 0.5;
        this.weapon3 = game.add.sprite(745, 545, 'weapon3');
        this.weapon3.alpha = 0.5;
        
        ///Setup cursor
        this.cursors = game.input.keyboard.createCursorKeys();
        this.fireBtn = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.skill1 = game.input.keyboard.addKey(Phaser.Keyboard.Z);
        this.skill2 = game.input.keyboard.addKey(Phaser.Keyboard.X);
        this.skill3 = game.input.keyboard.addKey(Phaser.Keyboard.C);
    },
    update: function () {
        ///background scroll
        this.background.tilePosition.y += 2;
        ///generate buffs
        this.generateBuff();
        ///generate enemies
        if (this.EnemyLevel < this.MaxEnemyPerLevel.length){
            if (!this.AlreadyStart) {
                this.startTween.start();
                this.startTween.onComplete.add(()=>{
                    this.AlreadyStart = true;
                    this.EnemyAppearNUM = 0;
                    
                    this.EnemyFireTime.fill(game.time.now + 2000);
                    this.generateEnemies;
                }, this);
            }
            else {
                ///generateEnemy
                if (this.EnemyAppearNUM < this.MaxEnemyPerLevel[this.EnemyLevel]) {this.generateEnemies();}
                ///enemy out of bounds
                this.enemies.forEachAlive(enemy => {if (enemy.y > 616) {enemy.kill();}}, this);

                ///control of boss
                if (this.bossReady) {
                    this.boss.body.velocity.x = (this.bossRL) ? 30 : -30;
                    this.boss.body.velocity.y = (this.bossUD) ? 20 : -20;
                    if (this.boss.x < 100) {this.boss.x = 100;this.bossRL = !this.bossRL;}
                    else if (this.boss.x > 700) {this.boss.x = 700;this.bossRL = !this.bossRL;}
                    if (this.boss.y < 200) {this.boss.y = 200;this.bossUD = !this.bossUD;}
                    else if (this.boss.y > 300) {this.boss.y = 300;this.bossUD = !this.bossUD;}
                }
                else {
                    if (this.boss.y >= 250) {
                        this.boss.body.velocity.y = 0;
                        this.bossReady = true;
                        this.BossFireTime = game.time.now + 100;
                    }
                }
                if (this.enemies.countLiving() == 0 && !this.boss.alive && this.EnemyAppearNUM == this.MaxEnemyPerLevel[this.EnemyLevel] && this.player.alive) {
                    this.AlreadyStart = false;
                    this.EnemyLevel++;
                    this.start.text = 'Level ' + (this.EnemyLevel+1);
                }
            }
        }

        if (this.player.alive || this.lives.countLiving()) {
            ///control of player
            this.player.body.velocity.setTo(0, 0);
            if (this.cursors.left.isDown && this.player.body.x > 0) {
                this.player.scale.x = -1;
                this.player.children[0].x = 17;
                this.player.children[1].x = 15;
                this.player.children[0].scale.x = (this.player.children[0].scale.x > 0) ? -this.player.children[0].scale.x : this.player.children[0].scale.x;
                this.player.children[1].scale.x = (this.player.children[1].scale.x > 0) ? -this.player.children[1].scale.x : this.player.children[1].scale.x;
                this.player.body.velocity.x = -200;
            }
            else if (this.cursors.right.isDown && this.player.body.x < 800 - this.player.width) {
                this.player.scale.x = 1;
                this.player.children[0].x = -17;
                this.player.children[1].x = -15;
                this.player.children[0].scale.x = (this.player.children[0].scale.x > 0) ? this.player.children[0].scale.x : -this.player.children[0].scale.x;
                this.player.children[1].scale.x = (this.player.children[1].scale.x > 0) ? this.player.children[1].scale.x : -this.player.children[1].scale.x;
                this.player.body.velocity.x = 200;
            }
            if (this.cursors.up.isDown && this.player.body.y > 0) {this.player.body.velocity.y = -200;}
            else if (this.cursors.down.isDown && this.player.body.y < 600 - this.player.height) {this.player.body.velocity.y = 200;}

            ///player fire
            if (this.fireBtn.isDown) {this.playerFire();}
            ///enemy fire
            if (this.enemies.countLiving() != 0) {this.enemyFire();}
            ///boss fire
            if (this.bossReady && this.boss.alive) {this.bossFire();}

            ///skill open
            if (this.coin >= 200) {this.weapon1.alpha = 1;}
            else {this.weapon1.alpha = 0.5;}
            if (this.coin >= 400) {this.weapon2.alpha = 1;}
            else {this.weapon2.alpha = 0.5;}
            if (this.coin >= 600) {this.weapon3.alpha = 1;}
            else {this.weapon3.alpha = 0.5;}
            ///weapon open
            if (this.skill1.isDown && !this.weaponON[0]) {if (this.weapon1.alpha == 1) {this.weaponOpen(0);}}
            if (this.skill2.isDown && !this.weaponON[1]) {if (this.weapon2.alpha == 1) {this.weaponOpen(1);}}
            if (this.skill3.isDown && !this.weaponON[2]) {if (this.weapon3.alpha == 1) {this.weaponOpen(2);}}
            ///weapon attack
            if (this.weaponON[0]) {this.weaponAttack(0);}
            if (this.weaponON[1]) {this.weaponAttack(1);}
            if (this.weaponON[2]) {this.weaponAttack(2);}
            ///close weapon
            if (game.time.now > this.weaponTimer[0]) {this.weaponClose(0);}
            if (game.time.now > this.weaponTimer[1]) {this.weaponClose(1);}
            if (game.time.now > this.weaponTimer[2]) {this.weaponClose(2);}

            //auto aiming of rocket
            this.Rocket.forEachAlive(rocket => {
                var enemy = this.nearestEnemy(rocket.x, rocket.y);
                if (enemy) {this.autoAiming(rocket, enemy, 300);}
                if (this.boss.alive) {this.autoAiming(rocket, this.boss, 300);}
            })

            ///Get Buff
            game.physics.arcade.overlap(this.player, this.recoveryBuff, this.RecoverPlayer, null, this);
            game.physics.arcade.overlap(this.player, this.shieldBuff, this.OpenShield, null, this);
            game.physics.arcade.overlap(this.player, this.coinBuff, this.GetCoins, null, this);

            ///close shieldArea
            if (game.time.now > this.ShieldTime) {this.shieldArea.visible = false;}
            
            ///enemy been attack
            game.physics.arcade.overlap(this.enemies, this.bulletsFriend, this.bulletHitEnemy, null, this);
            game.physics.arcade.overlap(this.enemies, this.GunBullet, this.GunHitEnemy, null, this);
            game.physics.arcade.overlap(this.LaserBullet1, this.enemies, this.LaserHitEnemy, null, this);
            game.physics.arcade.overlap(this.LaserBullet2, this.enemies, this.LaserHitEnemy, null, this);
            game.physics.arcade.overlap(this.enemies, this.Rocket, this.RocketHitEnemy, null, this);

            ///boss been attack
            if (this.bossReady && this.boss.alive) {
                game.physics.arcade.overlap(this.LaserBullet1, this.boss.children[0], this.LaserHitBoss, null, this);
                game.physics.arcade.overlap(this.LaserBullet2, this.boss.children[0], this.LaserHitBoss, null, this);
                ///bottom body
                game.physics.arcade.overlap(this.boss.children[0], this.bulletsFriend, this.bulletHitBoss, null, this);
                game.physics.arcade.overlap(this.boss.children[0], this.GunBullet, this.GunHitBoss, null, this);
                game.physics.arcade.overlap(this.boss.children[0], this.Rocket, this.RocketHitBoss, null, this);
                ///middle body
                game.physics.arcade.overlap(this.boss.children[1], this.bulletsFriend, this.bulletHitBoss, null, this);
                game.physics.arcade.overlap(this.boss.children[1], this.GunBullet, this.GunHitBoss, null, this);
                game.physics.arcade.overlap(this.boss.children[1], this.Rocket, this.RocketHitBoss, null, this);
                ///top body
                game.physics.arcade.overlap(this.boss.children[2], this.bulletsFriend, this.bulletHitBoss, null, this);
                game.physics.arcade.overlap(this.boss.children[2], this.GunBullet, this.GunHitBoss, null, this);
                game.physics.arcade.overlap(this.boss.children[2], this.Rocket, this.RocketHitBoss, null, this);
            }

            ///player been attack
            if (this.playerReady) {
                game.physics.arcade.overlap(this.player, this.bulletsEnemy, this.bulletHitPlayer, null, this);
                game.physics.arcade.overlap(this.player, this.enemies, this.playerHitEnemy, null, this);
                game.physics.arcade.overlap(this.player, this.bulletsBoss, this.bulletHitPlayer, null, this);
                
                if (this.bossReady && this.boss.alive) {
                    game.physics.arcade.overlap(this.player, this.boss.children[0], this.playerHitBoss, null, this);
                    game.physics.arcade.overlap(this.player, this.boss.children[1], this.playerHitBoss, null, this);
                    game.physics.arcade.overlap(this.player, this.boss.children[2], this.playerHitBoss, null, this);
                }
            }
            else {this.playerReadyTween.start();}

            ///shield been attack
            if (this.shieldArea.visible) {
                game.physics.arcade.overlap(this.shieldArea, this.bulletsEnemy, this.bulletHitShield, null, this);
                game.physics.arcade.overlap(this.shieldArea, this.bulletsBoss, this.bulletHitShield, null, this);
            }
        }
        else {
            FinalScore = this.score;
            this.GameEnd(0);
        }
        ///win
        if (this.EnemyLevel == 4) {
            FinalScore = this.score;
            this.GameEnd(1);
        }

        if (game.input.mousePointer.isDown && this.END) {game.state.start('menu');}
    },
    render: function () {
    },
    
    createBuff: function (item, name, type) {
        item.enableBody = true;
        item.physicsBodyType = Phaser.Physics.ARCADE;
        item.createMultiple(this.MaxNumPerBuff[type], name);
        item.setAll('anchor.x', 0.5);
        item.setAll('anchor.y', 0.5);
        item.setAll('outOfBoundsKill', true);
        item.setAll('checkWorldBounds', true);
    },
    createBullet: function (item, name, number) {
        item.enableBody = true;
        item.physicsBodyType = Phaser.Physics.ARCADE;
        item.createMultiple(number, name);
        item.setAll('anchor.x', 0.5);
        item.setAll('anchor.y', 0.5);
        item.setAll('checkWorldBounds', true);
        item.setAll('outOfBoundsKill', true);
    },
    createBloodStrip: function (item, string) {
        var healthstrip = game.add.sprite(0, 0, 'healthStrip');
        (string === 'boss') ? healthstrip.scale.setTo(2,2) : healthstrip.scale.setTo(1,1);
        healthstrip.anchor.setTo(0, 0.5);
        healthstrip.alpha = 0.7;
        var bloodstrip = game.add.sprite(0, 0, 'bloodStrip');
        (string === 'boss') ? bloodstrip.scale.setTo(3,2) : bloodstrip.scale.setTo(1.5,1);
        bloodstrip.anchor.setTo(0, 0.5);
        bloodstrip.alpha = 0.7;

        switch (string) {
            case 'player' : healthstrip.reset(-17, -25);bloodstrip.reset(-15, -25); break;
            case 'enemy' : healthstrip.reset(-16, -16);bloodstrip.reset(-14, -16); break;
            default : healthstrip.reset(-35, -120);bloodstrip.reset(-31, -120); break;
        }

        item.addChild(healthstrip, 'healthstrip');
        item.addChild(bloodstrip, 'bloodstrip');
    },
    createPlayer: function (player) {
        game.physics.enable(player, Phaser.Physics.ARCADE);
        player.anchor.setTo(0.5, 0.5);
        player.health = 100;
        this.createBloodStrip(player, 'player');

        var ship = game.add.sprite(0, 0, 'ship');
        ship.anchor.setTo(0.5, 0.5);
        ship.animations.add('ship');
        ship.play('ship', 4, true, true);
        player.addChild(ship, 'ship');
    },
    createEnemy: function (enemy) {
        enemy.scale.setTo(1.25, 1.25);
        enemy.anchor.setTo(0.5, 0.5);
        enemy.checkWorldBounds = true;
        enemy.outOfBoundsKill = true;
        enemy.animations.add('enemy');
        this.createBloodStrip(enemy, 'enemy');
    },
    createBoss: function (boss) {
        boss.health = 2000;
        game.physics.enable(boss, Phaser.Physics.ARCADE);
        for (var i = 0 ; i < 3; i++){
            switch (i) {
                case 0: var body = game.add.sprite(0, -152, 'bossbody' + (i+1));break;
                case 1: var body = game.add.sprite(0, -92, 'bossbody' + (i+1));break;
                default: var body = game.add.sprite(0, 0, 'bossbody' + (i+1));break;
            }
            body.anchor.setTo(0.5, 1);
            body.scale.setTo(2,2);
            game.physics.enable(body, Phaser.Physics.ARCADE);
            boss.addChild(body, i);
        }
        this.createBloodStrip(boss, 'boss');
    },
    generateEnemies: function () {
        if (game.time.now > this.EnemyGenerateTime) {
            switch (this.EnemyLevel) {
                case 0: 
                    for (var i = 0 ; i < 10 ; i++) {
                        ///reset enemy
                        var enemy = this.enemies.children[this.EnemyAppearNUM%70];
                        enemy.reset(i*60+90, -16);
                        enemy.health = 100;
                        enemy.play('enemy', 20, true, true);

                        enemy.body.velocity.x = 0;
                        enemy.body.velocity.y = 10;
                        enemy.body.acceleration.x = 0;
                        enemy.body.acceleration.y = 5;
                            
                        enemy.children[0].reset(-16, -16);
                        enemy.children[1].reset(-14, -16);
                        enemy.children[1].scale.x = 1.5;

                        this.EnemyAppearNUM++;
                        this.EnemyGenerateTime = 10000 + game.time.now;   
                    }
                    break;
                case 1:
                    for (var i = 0 ; i < 2 ; i++) {
                        ///reset enemy
                        var enemy = this.enemies.children[this.EnemyAppearNUM%70];
                        switch (this.EnemyAppearNUM%10) {
                            case 0:;case 1:(i) ? enemy.reset(400-32, -16) : enemy.reset(400+32, -16);break;
                            case 2:;case 3: (i) ? enemy.reset(350-32, -16) : enemy.reset(450+32, -16);break;
                            case 4:;case 5: (i) ? enemy.reset(300-32, -16) : enemy.reset(500+32, -16);break;
                            case 6:;case 7: (i) ? enemy.reset(250-32, -16) : enemy.reset(550+32, -16);break;
                            default:(i) ? enemy.reset(200-32, -16) : enemy.reset(600+32, -16);break;
                        }
                        enemy.health = 100;
                        enemy.play('enemy', 20, true, true);

                        enemy.body.velocity.x = 0;
                        enemy.body.velocity.y = 30;
                        enemy.body.acceleration.x = 0;
                        enemy.body.acceleration.y = 0;
                            
                        enemy.children[0].reset(-16, -16);
                        enemy.children[1].reset(-14, -16);
                        enemy.children[1].scale.x = 1.5;

                        this.EnemyAppearNUM++;
                        this.EnemyGenerateTime = 2500 + game.time.now;   
                    }
                    break;
                case 2:
                    ///generate enemy
                    var enemy = this.enemies.getFirstExists(false);
                    enemy.reset(game.rnd.between(16, 800-16), -16);
                    enemy.health = 100;
                    enemy.play('enemy', 20, true, true);           
            
                    enemy.body.velocity.x = 0;
                    enemy.body.velocity.y = 10;
                    enemy.body.acceleration.x = game.rnd.between(-20, 20);
                    enemy.body.acceleration.y = game.rnd.between(20, 50);
            
                    enemy.children[0].reset(-16, -16);
                    enemy.children[1].reset(-14, -16);
                    enemy.children[1].scale.x = 1.5;
                        
                    this.EnemyAppearNUM ++;
                    this.EnemyGenerateTime = game.rnd.between(100, 700) + game.time.now;
                    break;
                case 3:
                    this.boss.reset(400, 0);
                    this.boss.health = 10000;

                    this.boss.body.velocity.x = 0;
                    this.boss.body.velocity.y = 100;
                    
                    this.EnemyAppearNUM++;
                    break;
                default: break;
            }
        }
        
    },
    generateBuff: function () {
        if (game.time.now > this.BuffGenerateTime) {
            var selector = game.rnd.integerInRange(1,10);
            var buff;
            switch (selector) {
                case 1 : buff = this.recoveryBuff.getFirstExists(false);break;
                case 2 : buff = this.shieldBuff.getFirstExists(false);break;
                default : buff = this.coinBuff.getFirstExists(false);break;
            }
            if (buff) {
                buff.reset(game.rnd.between(32,800-32), -10);
                buff.body.velocity.y = game.rnd.between(100,300);
                if (selector > 2) {buff.play('coin', 5, true, true);}
                this.BuffGenerateTime = game.time.now + game.rnd.between(1000,4000);
            }
        }
    },

    playerFire: function () {
        if (game.time.now > this.PlayerFireTimer) {
            this.FireAudio.play();
            if (this.level == 0) {this.playerFireStage(0, 700);}
            else if (this.level <= 3) {this.playerFireStage(0, 500);}
            else if (this.level <= 6) {this.playerFireStage(1, 500);}
            else if (this.level <= 9) {this.playerFireStage(1, 300);}
            else if (this.level == 10) {this.playerFireStage(2, 100);}
        }
    },
    playerFireStage: function (stage, speed) {
        switch (stage) {
            case 1 : 
                var bullet1 = this.bulletsFriend.getFirstExists(false);
                bullet1.reset(this.player.x+10, this.player.body.y);
                bullet1.body.velocity.y = -400;

                var bullet2 = this.bulletsFriend.getFirstExists(false);
                bullet2.reset(this.player.x-10, this.player.body.y);
                bullet2.body.velocity.y = -400;
                break;
            case 2 : 
                var bullet1 = this.bulletsFriend.getFirstExists(false);
                bullet1.reset(this.player.x-10, this.player.body.y);
                bullet1.body.velocity.x = -75;
                bullet1.body.velocity.y = -400;
                bullet1.angle = -Math.asin(bullet1.body.velocity.x/bullet1.body.velocity.y)*180/Math.PI;

                var bullet2 = this.bulletsFriend.getFirstExists(false);
                bullet2.reset(this.player.x-10, this.player.body.y);
                bullet2.body.velocity.y = -400;
                bullet2.angle = 0;

                var bullet3 = this.bulletsFriend.getFirstExists(false);
                bullet3.reset(this.player.x+10, this.player.body.y);
                bullet3.body.velocity.y = -400;
                bullet3.angle = 0;

                var bullet4 = this.bulletsFriend.getFirstExists(false);
                bullet4.reset(this.player.x+10, this.player.body.y);
                bullet4.body.velocity.x = 75;
                bullet4.body.velocity.y = -400;
                bullet4.angle = Math.asin(bullet1.body.velocity.x/bullet1.body.velocity.y)*180/Math.PI;
                break;
            default : 
                var bullet = this.bulletsFriend.getFirstExists(false);
                bullet.reset(this.player.x, this.player.body.y);
                bullet.body.velocity.y = -400;
                break;
        }
        this.PlayerFireTimer = game.time.now + speed;
    },
    weaponOpen: function (state) {
        this.weaponTimer[state] = game.time.now + 6000;
        switch (state) {
            case 0 : this.GetCoin(-200);this.weaponON[0]=true;this.Gun1.visible=this.Gun2.visible=true;break;
            case 1 : this.GetCoin(-400);this.weaponON[1]=true;this.Laser1.visible=this.Laser2.visible=true;break;
            default: this.GetCoin(-600);this.weaponON[2]=true;break;
        }
    },
    weaponAttack: function (state) {
        switch (state) {
            case 0 : 
                if (game.time.now > this.GunFireTimer) {
                    var bullet1 = this.GunBullet.getFirstExists(false);
                    bullet1.reset(this.Gun1.x + this.player.x, this.Gun1.y -20 + this.player.y);
                    bullet1.body.velocity.y = -400;

                    var bullet2 = this.GunBullet.getFirstExists(false);
                    bullet2.reset(this.Gun2.x + this.player.x, this.Gun2.y -20 + this.player.y);
                    bullet2.body.velocity.y = -400;
                    this.GunFireTimer = game.time.now + 100;
                }
                break;
            case 1 : 
                if (game.time.now > this.LaserFireTimer) {
                    this.LaserBullet1.reset(15, 0);
                    this.LaserBullet2.reset(15, 0);
                    this.LaserBullet1.play('LaserBullet', 5, false, true);
                    this.LaserBullet2.play('LaserBullet', 5, false, true);
                    this.LaserFireTimer = game.time.now + 2000;
                }
                break;
            default: 
                if (game.time.now > this.RocketFireTimer) {
                    var rocket = this.Rocket.getFirstExists(false);
                    var enemy = this.enemies.getFirstAlive();
                    if (enemy) {
                        rocket.reset(this.player.x, this.player.y-20);
                        game.physics.arcade.moveToObject(rocket,enemy,300);
                        this.RocketFireTimer = game.time.now + 2000;
                    }
                    if (this.boss.alive) {
                        rocket.reset(this.player.x, this.player.y-20);
                        game.physics.arcade.moveToObject(rocket,this.boss,300);
                        this.RocketFireTimer = game.time.now + 2000;
                    }
                }
                break;
        }
    },
    weaponClose: function (state) {
        switch (state) {
            case 0 : this.weaponON[0] = false;this.Gun1.visible = this.Gun2.visible = false;break;
            case 1 : this.weaponON[1] = false;this.Laser1.visible = this.Laser2.visible = false;break;
            default: this.weaponON[2] = false;break;
        }
    },
    nearestEnemy: function (x, y) {
        var nearestEnemy = null;
        var minDistance = Math.pow(800, 2) + Math.pow(600, 2);
        this.enemies.forEachAlive(enemy => {
            var distance = Math.pow(enemy.x - x, 2) + Math.pow(enemy.y - y, 2);
            if (minDistance > distance) {
                minDistance = distance;
                nearestEnemy = enemy;
            }
        })
        return nearestEnemy;
    },
    autoAiming: function (rocket, enemy, speed) {
        var angle = Math.atan( rocket.body.velocity.x / (-rocket.body.velocity.y) ) * 180/Math.PI;
        rocket.angle = (rocket.body.velocity.y < 0) ? angle : 180+angle;
        game.physics.arcade.moveToObject(rocket, enemy, speed);
    },
    enemyFire: function () {
        var speed = 100;
        var firetime = 5000;
        switch (this.EnemyLevel) {
            case 0: speed = 200;firetime = 4000;break;
            case 1: speed = 200;firetime = 2500;break;
            case 2: speed = 100;firetime = 5000;break;
            default:speed = 000;firetime = 0000;break;
        }
        for (var i = 0 ; i < this.enemies.length ; i++) {
            if (game.time.now > this.EnemyFireTime[i] && this.enemies.children[i].alive) {
                var bullet = this.bulletsEnemy.getFirstExists(false);
                if (bullet) {
                    bullet.reset(this.enemies.children[i].x+16, this.enemies.children[i].y+16);
                    game.physics.arcade.moveToObject(bullet,this.player,speed);
                    this.EnemyFireTime[i] = game.time.now + firetime;
                }
            }
        }
    },
    bossFire: function () {
        var speed = 100;
        var firetime = 2000;
        if (game.time.now > this.BossFireTime) {
            for (var i = 0 ; i < 2; i++) {
                for (var j = 0 ; j < 3 ; j++) {
                    var bullet = this.bulletsBoss.getFirstExists(false);
                    if (i) {bullet.reset(this.boss.x-85, this.boss.y+this.boss.children[1].y-30);} 
                    else {bullet.reset(this.boss.x+85, this.boss.y+this.boss.children[1].y-30);}
                    bullet.play('bossbullet', 30, true, true);
                    bullet.body.velocity.x = (j*30)-30;
                    bullet.body.velocity.y = 100;
                    bullet.angle = Math.atan(-bullet.body.velocity.x/bullet.body.velocity.y)*180/Math.PI
                }   
            }
            this.BossFireTime = game.time.now + firetime;
        }
    },

    boom: function (x, y) {
        this.BoomAudio.play();
        var explosion = this.explosions.getFirstExists(false);
        explosion.reset(x, y);
        explosion.play('kaboom', 30, false, true);
    },
    BOSSboom: function (x, y, time) {
        this.explosionTime = game.time.now + 10;
        this.BoomAudio.play();
        var explosion = this.explosions.getFirstExists(false);
        explosion.reset(game.rnd.between(x-159, x+159), game.rnd.between(y-340, y));
        explosion.play('kaboom', 30, false, true);
        if (time > 0) this.BOSSboom(x, y, time-1);
        else {this.Deadboss.kill();}
    },
    playEmitter: function (x, y) {
        this.emitter.x = x;
        this.emitter.y = y;
        this.emitter.start(true, 500, null, 30);
    },
    enemyBeenHit: function (damage, enemy, bullet, type) {
        this.BeenHitAudio.play();
        enemy.damage(damage);      ///damage enemy
        enemy.children[1].scale.x = enemy.health/100*1.5;               ///updata blood strip
        if (enemy.health <= 0) {
            this.boom(enemy.x, enemy.y);    ///generate explosion
            (type==3) ? this.GetScore(-100) : this.GetScore(100);    ///get score
            (type==3) ? null : this.GetEXP(3);        ///get EXP
            (type==3) ? null : this.GetCoin(20);      ///get coin
        }
        else {
            switch (type) {         ///play emitter
                case 1: this.playEmitter(enemy.x, enemy.y);break;
                case 2: this.boom(bullet.x, bullet.y);break;
                default: this.playEmitter(bullet.x, bullet.y-bullet.height/2);break;
            }
        }
    },
    bossBeenHit: function (damage, boss, bullet, type) {
        this.BeenHitAudio.play();
        this.boss.damage(damage);   ///damage boss
        this.boss.children[4].scale.x = this.boss.health/10000*3;       ///updata blood strip
        if (this.boss.health <= 0) {
            this.Deadboss.reset(this.boss.x, this.boss.y, 15);
            this.BOSSboom(this.boss.x, this.boss.y, 7);        ///generate explosion
            this.GetScore(2000);    ///get score
        }
        else {
            (type==3) ? this.GetScore(-100) : null;    ///get score
            switch (type) {         ///play emitter
                case 1: this.playEmitter(boss.x, boss.y);break;
                case 2: this.boom(bullet.x, bullet.y);break;
                default: this.playEmitter(bullet.x, bullet.y-bullet.height/2);break;
            }
        }
    },
    bulletHitEnemy: function (enemy, bullet) {
        bullet.kill();  ///kill bullet
        this.enemyBeenHit(50, enemy, bullet, 0);
    },
    bulletHitBoss: function (boss, bullet) {
        bullet.kill();  ///kill bullet
        this.bossBeenHit(50, boss, bullet, 0);
    },
    
    GunHitEnemy: function (enemy, bullet) {
        bullet.kill();  ///kill bullet
        this.enemyBeenHit(10, enemy, bullet, 0);
    },
    GunHitBoss: function (boss, bullet) {
        bullet.kill();  ///kill bullet
        this.bossBeenHit(10, boss, bullet, 0)
    },
    
    LaserHitEnemy: function (laser, enemy) {
        this.enemyBeenHit(5, enemy, laser, 1);
    },
    LaserHitBoss: function (laser, boss) {
        this.bossBeenHit(5, boss, laser, 1);
    },
    
    RocketHitEnemy: function (enemy, bullet) {
        bullet.kill();  ///kill bullet
        this.enemyBeenHit(100, enemy, bullet, 2);
    },
    RocketHitBoss: function (boss, bullet) {
        bullet.kill();  ///kill bullet
        this.bossBeenHit(100, boss, bullet, 2);
    },
    
    playerBeenHit: function (damage, player, bullet) {
        this.BeenHitAudio.play();
        if (this.EnemyLevel<this.MaxEnemyPerLevel.length) {
            player.damage(damage); ///damage player
            player.children[1].scale.x = (player.children[1].scale.x > 0) ? player.health/100*1.5 :- player.health/100*1.5;  ///updata blood strip
            if (this.player.health <= 0) {
                this.boom(player.x, player.y);    ///generate explosion
                this.lives.getFirstAlive().kill();          ///kill lives
                this.playerReady = false;                   ///Reready
                if (this.lives.countLiving() != 0) {
                    player.reset(400, 500);            ///reset player
                    player.health = 100;
                    player.children[1].scale.x = (player.children[1].scale.x > 0) ? player.health/100*1.5 : -player.health/100*1.5;
                }
            }
            else {this.playEmitter(bullet.x, bullet.y);}    ///play emitter
        }
    },    
    playerHitEnemy: function (player, enemy) {
        this.enemyBeenHit(100, enemy, player, 3);
        this.playerBeenHit(100, player, enemy);   ///kill player live
    },
    playerHitBoss: function (player, boss) {
        this.bossBeenHit(200, boss, player, 3);
        this.playerBeenHit(100, player, boss);   ///kill player live
    },    
    bulletHitPlayer: function (player, bullet) {
        bullet.kill();  ///kill bullet
        this.playerBeenHit(20, player, bullet);   ///kill player live
    },

    bulletHitShield: function (shieldArea, bullet) {
        if (shieldArea.visible == true) {bullet.kill();}
    },
    RecoverPlayer: function (player, buff) {
        buff.kill();    ///kill buff
        ///recover lives
        if (Math.abs(this.player.children[1].scale.x) < 0.75) {
            this.player.children[1].scale.x += (player.children[1].scale.x > 0) ? 0.75 : -0.75;
        }
        else if (Math.abs(this.player.children[1].scale.x) >= 0.75 && Math.abs(this.player.children[1].scale.x) < 1.5) {
            this.player.children[1].scale.x = (player.children[1].scale.x > 0) ? 1.5 : -1.5;
        }
        
        this.RecoverAudio.play();   ///play audio
    },
    OpenShield: function (player, buff) {
        buff.kill();    ///kill buff
        this.shieldArea.reset(0, 0);    //open shieldArea
        this.ShieldTime = game.time.now + 2500;  
        this.ShieldAudio.play();    ///play audio
    },
    GetCoins: function (player, buff) {
        buff.kill();    ///kill buff
        this.GetCoin(100);   ///get coin
        this.GetCoinAudio.play();   ///play audio
    },

    GetScore: function (get) {
        this.score += get;
        this.scoreText.text = this.scoreString + this.score;
    },
    GetCoin: function (get) {
        this.coin += get;
        this.coinText.text = this.coinString + this.coin;
    },
    GetEXP: function (get) {
        this.exp += Math.round(get/this.EXPperLevel[this.level]*100);
        if (this.level < 10) {
            if (this.exp >= 100) {
                this.exp = 0;
                this.level += 1;
                this.levelText.text =  this.levelString + this.level;
            }
            if (this.level == 10) this.expText.text =  this.expString + 'MAX';
            else this.expText.text =  this.expString + this.exp + '%';

        }
        else {
            this.expText.text =  this.expString + 'MAX';
        }
    },
    GameEnd: function (state) {
        this.END = true;
        if (state) {
            this.endText1.text = 'YOU WIN';
            this.endText1.visible = this.endText2.visible = true;
        }
        else {
            this.endText1.text = 'YOU LOSE';
            this.endText1.visible = this.endText2.visible = true;
        }
    }
}
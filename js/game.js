var game = new Phaser.Game(800, 600, Phaser.AUTO, 'gameDiv');

var FinalScore = 0;
var effectVolumn = 0.5;
var backgroundVolumn = 0.5;
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('single', singleState);
game.state.add('multi', multiState);
game.state.add('setting', settingState);

game.state.start('boot');
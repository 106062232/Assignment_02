var menuState = {
    create: function () {
        var majorStyle = {font:'50px Arial', fontWeight:'bold', stroke:'#AED4E6', strokeThickness:'10'};
        var minorStyle = {font:'40px Arial', fontWeight:'bold', stroke:'#AED4E6', strokeThickness:'10'};
        game.add.image(0, 0, 'menu_Background').scale.setTo(1,1);
        
        this.backMusic = game.add.audio('menu_BackgroundMusic', backgroundVolumn, true);
        this.backMusic.play();
        this.select = game.add.audio('menu_Select', effectVolumn, false);
        this.enter = game.add.audio('menu_Enter', effectVolumn, false);

        this.arrow  = game.add.image(40, 200, 'menu_Arrow', );
        this.arrow.scale.setTo(1,1.2);
        this.arrow.alpha = 0;
        this.arrow.visible = false;
        
        this.menuText = game.add.text(40, 120, 'MAIN MENU', majorStyle);
        var grd1 = this.menuText.context.createLinearGradient(0,0,this.menuText.width,0);
        grd1.addColorStop(0, '#00BFD1');
        grd1.addColorStop(0.35, '#5CF1FF');
        grd1.addColorStop(1, '#E7FDFF');
        this.menuText.fill = grd1;


        this.single = game.add.text(60, 200, 'Play', minorStyle);
        var grd2 = this.single.context.createLinearGradient(0,0,this.single.width,0);
        grd2.addColorStop(0, '#00BFD1');
        grd2.addColorStop(0.35, '#5CF1FF');
        grd2.addColorStop(1, '#E7FDFF');
        this.single.fill = grd2;
            this.single.inputEnabled = true;
        this.single.events.onInputOver.add(this.over, this);
        this.single.events.onInputOut.add(this.out, this);
        this.single.events.onInputDown.add(this.down, this);
/*
        this.multi = game.add.text(60, 250, 'Multiple', minorStyle);
        var grd3 = this.multi.context.createLinearGradient(0,0,this.multi.width,0);
        grd3.addColorStop(0, '#00BFD1');
        grd3.addColorStop(0.35, '#5CF1FF'); 
        grd3.addColorStop(1, '#E7FDFF');
        this.multi.fill = grd3;
        this.multi.inputEnabled = true;
        this.multi.events.onInputOver.add(this.over,this);
        this.multi.events.onInputOut.add(this.out, this);
        this.multi.events.onInputDown.add(this.down, this);
*/
        this.setting = game.add.text(60, 250, 'Setting', minorStyle);
        var grd4 = this.setting.context.createLinearGradient(0,0,this.setting.width,0);
        grd4.addColorStop(0, '#00BFD1');
        grd4.addColorStop(0.35, '#5CF1FF');
        grd4.addColorStop(1, '#E7FDFF');
        this.setting.fill = grd4;
        this.setting.inputEnabled = true;
        this.setting.events.onInputOver.add(this.over,this);
        this.setting.events.onInputOut.add(this.out, this);
        this.setting.events.onInputDown.add(this.down, this);
    },
    update: function () {
        this.backMusic.volumn = backgroundVolumn;
        this.select.volumn = effectVolumn;
        this.enter.volumn = effectVolumn;
    },
    over: function (item) {
        this.arrow.y = item.y;
        this.arrow.alpha = 0;
        this.arrow.visible = true;
        this.select.play();
        game.add.tween(this.arrow).to({ alpha: 1}, 300).start();
        game.add.tween(item).to({ x: 90}, 100).start();
    },
    out: function (item) {
        this.arrow.visible = false;
        game.add.tween(item).to({ x: 60}, 100).start();
    },
    down: function () {
        this.enter.play();
        this.backMusic.stop();
        switch(this.arrow.y) {
            case 200 : console.log('play');game.state.start('single');
                break;
            case 250 : console.log('setting');game.state.start('setting');
                break;
        }
    },
}
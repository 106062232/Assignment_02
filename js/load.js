var loadState = {
    preload: function () {
        ///menu
        game.load.image('menu_Background', 'assets/image/menu/background.jpg');
        game.load.image('menu_Arrow', 'assets/image/menu/arrow.png');
        game.load.audio('menu_BackgroundMusic', 'assets/audio/menu/BackgroundMusic.mp3');
        game.load.audio('menu_Select', 'assets/audio/menu/Select.mp3');
        game.load.audio('menu_Enter', 'assets/audio/menu/Enter.mp3');
        //play
        game.load.image('background', 'assets/image/play/background.png');
        game.load.image('friendbullet', 'assets/image/play/bullet/bullet2.png');
        game.load.image('enemybullet', 'assets/image/play/bullet/enemybullet.png');
        game.load.spritesheet('bossbullet', 'assets/image/play/bullet/bossbullet.png', 10, 36);
        game.load.image('bossbody1', 'assets/image/play/enemy/bossbody1.png');
        game.load.image('bossbody2', 'assets/image/play/enemy/bossbody2.png');
        game.load.image('bossbody3', 'assets/image/play/enemy/bossbody3.png');
        game.load.image('boss', 'assets/image/play/enemy/boss.png');
        game.load.image('player', 'assets/image/play/ship/player.png');
        game.load.image('heart', 'assets/image/play/health/heart.png');
        game.load.image('healthStrip', 'assets/image/play/health/healthStrip.png');
        game.load.image('bloodStrip', 'assets/image/play/health/bloodStrip.png');
        game.load.image('shield', 'assets/image/play/buff/shield.png');
        game.load.image('shieldArea', 'assets/image/play/buff/shieldArea.png');
        game.load.image('recovery', 'assets/image/play/buff/recovery.png');
        game.load.image('weapon1', 'assets/image/play/buff/weapon1.png');
        game.load.image('weapon2', 'assets/image/play/buff/weapon2.png');
        game.load.image('weapon3', 'assets/image/play/buff/weapon3.png');
        game.load.image('Gun', 'assets/image/play/weapon/Gun.png');
        game.load.image('GunBullet', 'assets/image/play/weapon/GunBullet.png');
        game.load.image('Laser', 'assets/image/play/weapon/Laser.png');
        game.load.spritesheet('LaserBullet', 'assets/image/play/weapon/LaserBullet.png', 27, 233);
        game.load.image('Rocket', 'assets/image/play/weapon/Rocket.png');
        game.load.image('pixel', 'assets/image/play/pixel.png');
        game.load.spritesheet('coin', 'assets/image/play/buff/coin.png', 23, 20);
        game.load.spritesheet('ship', 'assets/image/play/ship/ship.png', 53, 31);
        game.load.spritesheet('enemy', 'assets/image/play/enemy/invader32x32x4.png', 32, 32);
        game.load.spritesheet('kaboom', 'assets/image/play/explosion/explode.png', 128, 128);

        game.load.audio('BackgroundMusic', 'assets/audio/play/BackgrondMusic.mp3');
        game.load.audio('Fire', 'assets/audio/play/Fire.mp3');
        game.load.audio('FinalAttack', 'assets/audio/play/FinalAttack.mp3');
        game.load.audio('Boom', 'assets/audio/play/Boom.mp3');
        game.load.audio('Dead', 'assets/audio/play/Dead.mp3');
        game.load.audio('Recover', 'assets/audio/play/Recover.mp3');
        game.load.audio('Shield', 'assets/audio/play/Shield.mp3');
        game.load.audio('GetCoin', 'assets/audio/play/GetCoin.mp3');
        game.load.audio('BeenHit', 'assets/audio/play/BeenHit.mp3');
        ///setting
        game.load.image('gear1', 'assets/image/setting/gear1.png');
        game.load.image('gear2', 'assets/image/setting/gear2.png');
        game.load.image('gear3', 'assets/image/setting/gear3.png');
        game.load.image('bar', 'assets/image/setting/bar.png');
        game.load.image('dot', 'assets/image/setting/dot.png');
        game.load.audio('BackgroundTest', 'assets/audio/setting/BackgroundTest.mp3');
        game.load.audio('EffectTest', 'assets/audio/setting/EffectTest.mp3');
    },
    create: function () {
        game.state.start('menu');
    }
}
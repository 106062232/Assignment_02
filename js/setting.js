var settingState = {
    create: function () {
        this.BGcolor = game.stage.backgroundColor = '#AED4E6'
        this.BGtest = game.add.audio('BackgroundTest', backgroundVolumn, true).play();
        this.EFtest = game.add.audio('EffectTest', effectVolumn, false);

        this.gear1 = game.add.sprite(700-50, 300, 'gear1');
        this.gear1.anchor.setTo(0.5, 0.5);
        this.gear1.scale.setTo(2, 2);
        this.gear1.alpha = 0.7;

        this.gear2 = game.add.sprite(0-50, 600, 'gear2');
        this.gear2.anchor.setTo(0.5, 0.5);
        this.gear2.scale.setTo(3, 3);
        this.gear2.alpha = 0.1;

        this.gear3 = game.add.sprite(-50-50, 200, 'gear3');
        this.gear3.anchor.setTo(0.5, 0.5);
        this.gear3.scale.setTo(0.5, 0.5);
        this.gear3.alpha = 0.3;

        this.gear4 = game.add.sprite(50, 50, 'gear1');
        this.gear4.anchor.setTo(0.5, 0.5);
        this.gear4.scale.setTo(2, 2);
        this.gear4.alpha = 0.1;

        this.back = game.add.text(72, 528, 'B', { font: '50px Arial', fill: '#000'});
        this.back.anchor.setTo(0.5, 0.5);
        this.back.alpha = 0.4;

        this.gear5 = game.add.sprite(72, 528, 'gear2');
        this.gear5.anchor.setTo(0.5, 0.5);
        this.gear5.scale.setTo(0.5, 0.5);
        this.gear5.inputEnabled = true;
        this.gear5.alpha = 0.4;
        this.gear5Rspeed = 0;
        this.gear5.events.onInputOver.add(()=>{this.gear5Rspeed = 1;game.add.tween(this.back).to({ alpha: 1}, 500).start();game.add.tween(this.gear5).to({ alpha: 1}, 500).start();},this);
        this.gear5.events.onInputOut.add(()=>{this.gear5Rspeed = 0;game.add.tween(this.back).to({ alpha: 0.4}, 500).start();game.add.tween(this.gear5).to({ alpha: 0.4}, 500).start();}, this);
        this.gear5.events.onInputDown.add(()=>{this.BGtest.stop();game.state.start('menu')}, this);

        game.add.text(20, 20, 'Setting', { font: '50px Arial', fill: '#fff'});
        game.add.text(650, 200, 'Background', { font: '35px Arial', fill: '#fff'}).anchor.setTo(0.5, 0.5);
        this.volumnNum1 = game.add.text(650, 230, backgroundVolumn, { font: '20px Arial', fill: '#fff'});
        this.volumnNum1.anchor.setTo(0.5, 0.5);
        game.add.text(650, 350, 'Effect', { font: '35px Arial', fill: '#fff'}).anchor.setTo(0.5, 0.5);
        this.volumnNum2 = game.add.text(650, 380, effectVolumn, { font: '20px Arial', fill: '#fff'});
        this.volumnNum2.anchor.setTo(0.5, 0.5);

        this.bar1 = game.add.image(650, 250, 'bar');
        this.bar1.anchor.setTo(0.5, 0.5);
        this.bar1.inputEnabled = true;
        this.bar1.events.onInputOver.add(this.over,this);
        this.bar1.events.onInputOut.add(this.out, this);
        this.overBar1 = false;

        this.bar2 = game.add.image(650, 400, 'bar')
        this.bar2.anchor.setTo(0.5, 0.5);
        this.bar2.inputEnabled = true;
        this.bar2.events.onInputOver.add(this.over,this);
        this.bar2.events.onInputOut.add(this.out, this);
        this.overBar2 = false;

        this.dot1 = game.add.image(200*backgroundVolumn+550, 250, 'dot');
        this.dot1.anchor.setTo(0.5, 0.5);
        this.dot1.alpha = 0.4;
        this.dot1.inputEnabled = true;
        this.dot1.events.onInputOver.add(this.over,this);
        this.dot1.events.onInputOut.add(this.out, this);
        this.moveDot1 = false;

        this.dot2 = game.add.image(200*effectVolumn+550, 400, 'dot');
        this.dot2.anchor.setTo(0.5, 0.5);
        this.dot2.alpha = 0.4;
        this.dot2.inputEnabled = true;
        this.dot2.events.onInputOver.add(this.over,this);
        this.dot2.events.onInputOut.add(this.out, this);
        this.moveDot2 = false;

    },
    update: function () {
        this.gear1.angle += 0.2;
        this.gear2.angle -= 0.1;
        this.gear3.angle += 0.2;
        this.gear4.angle += 0.1;
        this.gear5.angle += this.gear5Rspeed;

        if (game.input.activePointer.isDown) {
            if (this.overDot1) {this.moveDot1 = true;}
            else if (this.overDot2) {this.moveDot2 = true;this.EFtest.play()}
        }
        if (game.input.activePointer.isUp) {
            this.moveDot1 = false;
            this.moveDot2 = false;
        }

        if (this.moveDot1) {
            if (game.input.x < 550) this.dot1.x = 550;
            else if (game.input.x > 750) this.dot1.x = 750;
            else this.dot1.x = game.input.x;
            this.BGtest.volume = this.volumnNum1.text = backgroundVolumn = (this.dot1.x-550)/200;
        }
        else if (this.moveDot2) {
            if (game.input.x < 550) this.dot2.x = 550;
            else if (game.input.x > 750) this.dot2.x = 750;
            else this.dot2.x = game.input.x;
            this.EFtest.volume = this.volumnNum2.text = effectVolumn = (this.dot2.x-550)/200;
            this.EFtest.play()
        }
    },

    over: function (item) {
        if (item.y < 300) {
            game.add.tween(this.dot1).to({ alpha: 1}, 500).start();
            this.overDot1 = true;
        }
        else {
            game.add.tween(this.dot2).to({ alpha: 1}, 500).start();
            this.overDot2 = true;
        }
    },
    out: function (item) {
        if (item.y < 300) {
            if (!this.moveDot1) {game.add.tween(this.dot1).to({ alpha: 0.4}, 500).start();}
            this.overDot1 = false;
        }
        else {
            if (!this.moveDot2) {game.add.tween(this.dot2).to({ alpha: 0.4}, 500).start();}
            this.overDot2 = false;
        }
    },
}
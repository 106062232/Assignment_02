var winState = {
    create: function () {
        game.stage.backgroundColor = '#ddd';
        this.background = game.add.image(game.world.centerX, game.world.centerY, 'winEnd');
        this.background.scale.setTo(0.5, 0.75);
        this.background.anchor.setTo(0.5,0.65);
        
        this.score = game.add.text(game.world.centerX, 470, 'Score: ' + FinalScore, { font: '70px Arial', fill: '#fff'});
        this.score.anchor.setTo(0.5,0.5);

        this.text = game.add.text(game.world.centerX, 550, '點擊滑鼠左鍵回到起始畫面', { font: '35px Arial', fill: '#fff'});
        this.text.fontWeight = 'bold';
        this.text.anchor.setTo(0.5,0.5);

        game.add.audio('winAudio', 1, false).play();
    },

    update: function () {
        if (game.input.mousePointer.isDown) {game.state.start('menu');}
    },
}

var loseState = {
    create: function () {
        game.stage.backgroundColor = '#999';
        this.background = game.add.image(game.world.centerX, game.world.centerY, 'loseEnd');
        this.background.scale.setTo(0.5, 0.35);
        this.background.anchor.setTo(0.5,0.75);
        
        this.score = game.add.text(game.world.centerX, 470, 'Score: ' + FinalScore, { font: '70px Arial', fill: '#fff'});
        this.score.anchor.setTo(0.5,0.5);

        this.text = game.add.text(game.world.centerX, 550, '點擊滑鼠左鍵回到起始畫面', { font: '35px Arial', fill: '#fff'});
        this.text.fontWeight = 'bold';
        this.text.anchor.setTo(0.5,0.5);

        game.add.audio('loseAudio', 1, false).play();
    },
    update: function () {
        if (game.input.mousePointer.isDown) {game.state.start('menu');}
    },
}